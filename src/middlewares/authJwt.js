import db from "../config.js"
import jwt from "jsonwebtoken"
require("dotenv").config()

export const verifyToken = async (req, res, next) => {
  const secretKey = process.env.SECRET_KEY
  const token = req.headers["x-access-token"]
  if (!token)
    return res.status(403).json({ message: "No se proporciona ningún token" })

  try {
    const decoded = jwt.verify(token, secretKey)
    const user = await db.query("SELECT * FROM users WHERE id = $1", [
      decoded.id,
    ])
    req.user = user.rows[0]
    next()
  } catch (error) {
    return res.status(401).json({ message: "Ningún usuario encontrado" })
  }
}

export const isSupervisor = async (req, res, next) => {
  try {
    const userId = req.user.id

    const user = await db.query("SELECT * FROM users WHERE id = $1", [userId])

    if (user.rows.length === 0) {
      return res.status(404).json({ message: "Usuario no encontrado" })
    }

    const rolesQuery = await db.query(
      "SELECT role_id FROM user_roles WHERE user_id = $1",
      [userId]
    )

    const userRolesIds = rolesQuery.rows.map((role) => role.role_id)
    const rolesNamesQuery = await db.query(
      "SELECT name FROM roles WHERE id = ANY($1)",
      [userRolesIds]
    )
    const userRoles = rolesNamesQuery.rows.map((role) => role.name)

    if (userRoles.includes("supervisor")) {
      next()
    } else {
      return res.status(403).json({
        message: "Acceso no autorizado. Se requiere rol de supervisor",
      })
    }
  } catch (error) {
    console.error("Error al verificar rol de supervisor:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

export const isAdmin = async (req, res, next) => {
  try {
    const userId = req.user.id

    const user = await db.query("SELECT * FROM users WHERE id = $1", [userId])

    if (user.rows.length === 0) {
      return res.status(404).json({ message: "Usuario no encontrado" })
    }

    const rolesQuery = await db.query(
      "SELECT role_id FROM user_roles WHERE user_id = $1",
      [userId]
    )

    const userRolesIds = rolesQuery.rows.map((role) => role.role_id)
    const rolesNamesQuery = await db.query(
      "SELECT name FROM roles WHERE id = ANY($1)",
      [userRolesIds]
    )
    const userRoles = rolesNamesQuery.rows.map((role) => role.name)

    if (userRoles.includes("admin")) {
      next()
    } else {
      return res
        .status(403)
        .json({
          message: "Acceso no autorizado. Se requiere rol de administrador",
        })
    }
  } catch (error) {
    console.error("Error al verificar rol de administrador:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}
