import db from "../config.js"
import { v4 as uuid_generate_v4 } from "uuid"

const createRole = async (req, res) => {
  try {
    const { name } = req.body

    const existingRole = await db.query("SELECT * FROM roles WHERE name = $1", [
      name,
    ])

    if (existingRole.rows.length > 0) {
      return res.status(400).json({ message: "El rol ya existe" })
    }

    const roleId = uuid_generate_v4()

    const newRole = await db.query(
      "INSERT INTO roles (id, name) VALUES ($1, $2) RETURNING *",
      [roleId, name]
    )

    res.status(201).json(newRole.rows[0])
  } catch (error) {
    console.error("Error al crear rol:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const listRoles = async (req, res) => {
  try {
    const roles = await db.query("SELECT * FROM roles")
    res.status(200).json(roles.rows)
  } catch (error) {
    console.error("Error al listar roles:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const deleteRole = async (req, res) => {
  try {
    const roleId = req.params.id

    const existingRole = await db.query("SELECT * FROM roles WHERE id = $1", [
      roleId,
    ])

    if (existingRole.rows.length === 0) {
      return res.status(404).json({ message: "Rol no encontrado" })
    }

    await db.query("DELETE FROM roles WHERE id = $1", [roleId])

    res.status(200).json(`Rol con ID ${roleId} eliminado correctamente.`)
  } catch (error) {
    console.error("Error al borrar rol:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

module.exports = {
  createRole,
  listRoles,
  deleteRole,
}
