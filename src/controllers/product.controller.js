import db from "../config.js"
import { v4 as uuid_generate_v4 } from "uuid"

const createProduct = async (req, res) => {
  try {
    const { name, description, price } = req.body

    const existingProduct = await db.query(
      "SELECT * FROM products WHERE name = $1",
      [name]
    )
    if (existingProduct.rows.length > 0) {
      return res.status(400).json({ message: "El producto ya existe" })
    }

    const productId = uuid_generate_v4()

    const insertQuery = `
        INSERT INTO products (id, name, description, price)
        VALUES ($1, $2, $3, $4)
        RETURNING *
      `
    const newProduct = await db.query(insertQuery, [
      productId,
      name,
      description,
      price,
    ])

    res.status(201).json(newProduct.rows[0])
  } catch (error) {
    console.error("Error al crear producto:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const listProducts = async (req, res) => {
  try {
    const products = await db.query("SELECT * FROM products")
    res.status(200).json(products.rows)
  } catch (error) {
    console.error("Error al listar productos:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const getProductByName = async (req, res) => {
  try {
    const name = req.params.name
    const product = await db.query(
      "SELECT * FROM products WHERE LOWER(name) = LOWER($1)",
      [name]
    )

    if (product.rows.length === 0) {
      return res.status(404).json({ message: "Producto no encontrado" })
    }

    res.status(200).json(product.rows[0])
  } catch (error) {
    console.error("Error al obtener producto por nombre:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const updateProduct = async (req, res) => {
  try {
    const productId = req.params.id

    const existingProduct = await db.query(
      "SELECT * FROM products WHERE id = $1",
      [productId]
    )

    const old = existingProduct.rows[0]

    const {
      name = old.name,
      description = old.description,
      price = old.price,
    } = req.body

    if (existingProduct.rows.length === 0) {
      return res.status(404).json({ message: "Producto no encontrado" })
    }

    const updatedProduct = await db.query(
      "UPDATE products SET name = $1, description = $2, price = $3 WHERE id = $4 RETURNING *",
      [name, description, price, productId]
    )

    res.status(200).json(updatedProduct.rows[0])
  } catch (error) {
    console.error("Error al actualizar producto:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const deleteProduct = async (req, res) => {
  try {
    const productId = req.params.id

    const existingProduct = await db.query(
      "SELECT * FROM products WHERE id = $1",
      [productId]
    )

    if (existingProduct.rows.length === 0) {
      return res.status(404).json({ message: "Producto no encontrado" })
    }

    await db.query("DELETE FROM products WHERE id = $1", [productId])

    res
      .status(200)
      .json(`Producto con ID ${productId} eliminado correctamente.`)
  } catch (error) {
    console.error("Error al borrar producto:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

module.exports = {
  createProduct,
  listProducts,
  updateProduct,
  deleteProduct,
  getProductByName,
}
