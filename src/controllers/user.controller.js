import db from "../config.js"
import bcrypt from "bcryptjs"
import jwt from "jsonwebtoken"

require("dotenv").config()
const signup = async (req, res) => {
  try {
    const { username, password, roles = ["employee"] } = req.body

    const existingUser = await db.query(
      "SELECT * FROM users WHERE username = $1",
      [username]
    )

    if (existingUser.rows.length > 0) {
      return res.status(400).json({ message: "El usuario ya existe" })
    }

    const hashedPassword = await bcrypt.hash(password, 10)

    const roleIds = await Promise.all(
      roles.map(async (roleName) => {
        const result = await db.query("SELECT id FROM roles WHERE name = $1", [
          roleName,
        ]);
        return result.rows[0] ? result.rows[0].id : undefined;
      })
    );
    

    if (roleIds.some((id) => id === undefined)) {
      return res
        .status(400)
        .json({ message: "Al menos uno de los roles proporcionados no existe" })
    }

    const newUser = await db.query(
      "INSERT INTO users (id, username, password, roles) VALUES (uuid_generate_v4(), $1, $2, $3) RETURNING *",
      [username, hashedPassword, roles[0]]
    )

    for (const roleId of roleIds) {
      await db.query(
        "INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)",
        [newUser.rows[0].id, roleId]
      )
    }

    res.status(201).json(newUser.rows[0])
  } catch (error) {
    console.error("Error al crear usuario:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}
const login = async (req, res) => {
  try {
    const { username, password } = req.body

    const user = await db.query("SELECT * FROM users WHERE username = $1", [
      username,
    ])

    if (user.rows.length === 0) {
      return res.status(404).json({ message: "Usuario no encontrado" })
    }

    const passwordMatch = await bcrypt.compare(password, user.rows[0].password)

    if (!passwordMatch) {
      return res.status(401).json({ message: "Contraseña incorrecta" })
    }

    const userId = user.rows[0].id

    const userRoles = await db.query(
      "SELECT role_id FROM user_roles WHERE user_id = $1",
      [userId]
    )

    const roleIds = userRoles.rows.map((row) => row.role_id)

    const roles = await db.query("SELECT name FROM roles WHERE id = ANY($1)", [
      roleIds,
    ])

    const roleNames = roles.rows.map((row) => row.name)

    const secretKey = process.env.SECRET_KEY
    const token = jwt.sign({ id: userId }, secretKey, {
      expiresIn: 86400,
    })

    res.status(200).json({
      message: "Inicio de sesión exitoso",
      token,
      user: { ...user.rows[0], roles: roleNames },
    })
  } catch (error) {
    console.error("Error al iniciar sesión:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const listUsers = async (req, res) => {
  try {
    const users = await db.query("SELECT * FROM users")
    res.status(200).json(users.rows)
  } catch (error) {
    console.error("Error al listar usuarios:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const updateUser = async (req, res) => {
  try {
    const userId = req.params.id

    const existingUser = await db.query("SELECT * FROM users WHERE id = $1", [
      userId,
    ])

    if (existingUser.rows.length === 0) {
      return res.status(404).json({ message: "Usuario no encontrado" })
    }

    const oldUser = existingUser.rows[0]

    const {
      username = oldUser.username,
      password = oldUser.password,
      roles = [oldUser.roles],
    } = req.body

    const hashedPassword = await bcrypt.hash(password, 10)

    const roleIds = await Promise.all(
      roles.map(async (roleName) => {
        const result = await db.query("SELECT id FROM roles WHERE name = $1", [
          roleName,
        ]);
        return result.rows[0] ? result.rows[0].id : undefined;
      })
    );
    

    if (roleIds.some((id) => id === undefined)) {
      return res
        .status(400)
        .json({ message: "Al menos uno de los roles proporcionados no existe" })
    }

    const updatedUser = await db.query(
      "UPDATE users SET username = $1, password = $2, roles = $3 WHERE id = $4 RETURNING *",
      [username, hashedPassword, roleIds, userId]
    )

    await db.query("DELETE FROM user_roles WHERE user_id = $1", [userId])

    for (const roleId of roleIds) {
      await db.query(
        "INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)",
        [userId, roleId]
      )
    }

    res.status(200).json(updatedUser.rows[0])
  } catch (error) {
    console.error("Error al actualizar usuario:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const getUserById = async (req, res) => {
  try {
    const userId = req.params.id

    const user = await db.query("SELECT * FROM users WHERE id = $1", [userId])

    if (user.rows.length === 0) {
      return res.status(404).json({ message: "Usuario no encontrado" })
    }

    res.status(200).json(user.rows[0])
  } catch (error) {
    console.error("Error al obtener usuario por ID:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const deleteUser = async (req, res) => {
  try {
    const userId = req.params.id

    await db.query("DELETE FROM user_roles WHERE user_id = $1", [userId])

    await db.query("DELETE FROM users WHERE id = $1", [userId])

    res.status(200).json(`Usuario con ID ${userId} eliminado correctamente.`)
  } catch (error) {
    console.error("Error al borrar usuario:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const assignRole = async (req, res) => {
  try {
    const userId = req.params.id
    const { roleId } = req.body

    await db.query(
      "INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)",
      [userId, roleId]
    )

    res
      .status(200)
      .json(`Rol asignado correctamente al usuario con ID ${userId}.`)
  } catch (error) {
    console.error("Error al asignar rol al usuario:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

module.exports = {
  signup,
  login,
  getUserById,
  listUsers,
  updateUser,
  deleteUser,
  assignRole,
}
