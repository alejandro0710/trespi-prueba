import db from "../config.js"

const createSale = async (req, res) => {
  try {
    const { productId, quantity } = req.body

    const product = await db.query(
      "SELECT id, name, description, price FROM products WHERE id = $1",
      [productId]
    )

    if (!product.rows.length) {
      return res.status(404).json({ message: "Producto no encontrado" })
    }

    const { id, name, description, price } = product.rows[0]

    const totalPrice = quantity * price

    const sale = await db.query(
      "INSERT INTO sales (product_id, quantity, total_price, created_at) VALUES ($1, $2, $3, NOW() AT TIME ZONE 'America/Bogota') RETURNING *",
      [id, quantity, totalPrice]
    )

    const formattedDate = new Intl.DateTimeFormat("es-CO", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }).format(new Date(sale.rows[0].created_at))

    res.status(201).json({
      ...sale.rows[0],
      productName: name,
      productDescription: description,
      formattedDate: formattedDate,
    })
  } catch (error) {
    console.error("Error al crear venta:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const listSales = async (req, res) => {
  try {
    const sales = await db.query(
      "SELECT sales.*, products.name as product_name, products.description as product_description FROM sales INNER JOIN products ON sales.product_id = products.id"
    )

    res.status(200).json(sales.rows)
  } catch (error) {
    console.error("Error al listar ventas:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const updateSale = async (req, res) => {
  try {
    const saleId = req.params.id
    const { quantity } = req.body

    const existingSale = await db.query("SELECT * FROM sales WHERE id = $1", [
      saleId,
    ])

    if (existingSale.rows.length === 0) {
      return res.status(404).json({ message: "Venta no encontrada" })
    }

    const productInfo = await db.query(
      "SELECT products.name AS product_name, products.description AS product_description, products.price FROM sales JOIN products ON sales.product_id = products.id WHERE sales.id = $1",
      [saleId]
    )

    if (productInfo.rows.length === 0) {
      return res
        .status(404)
        .json({ message: "Información del producto asociado no encontrada" })
    }

    const unitPrice = productInfo.rows[0].price

    const totalPrice = quantity * unitPrice

    const updatedSale = await db.query(
      "UPDATE sales SET quantity = $1, total_price = $2, created_at = NOW() AT TIME ZONE 'America/Bogota' WHERE id = $3 RETURNING *",
      [quantity, totalPrice, saleId]
    )

    updatedSale.rows[0].product_name = productInfo.rows[0].product_name
    updatedSale.rows[0].product_description =
      productInfo.rows[0].product_description

    const formattedDate = new Intl.DateTimeFormat("es-CO", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }).format(new Date(updatedSale.rows[0].created_at))

    updatedSale.rows[0].created_at = formattedDate

    res.status(200).json(updatedSale.rows[0])
  } catch (error) {
    console.error("Error al actualizar venta:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const deleteSale = async (req, res) => {
  try {
    const saleId = req.params.id

    const existingSale = await db.query("SELECT * FROM sales WHERE id = $1", [
      saleId,
    ])

    if (existingSale.rows.length === 0) {
      return res.status(404).json({ message: "Venta no encontrada" })
    }

    await db.query("DELETE FROM sales WHERE id = $1", [saleId])

    res.status(200).json(`Venta con ID ${saleId} eliminada correctamente.`)
  } catch (error) {
    console.error("Error al borrar venta:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const getSalesByDay = async (req, res) => {
  try {
    const { year, month, day } = req.params

    const sales = await db.query(
      `
      SELECT 
        sales.id AS sale_id,
        products.id AS product_id,
        products.name AS product_name,
        products.description AS product_description,
        products.price AS product_price,
        sales.quantity,
        sales.total_price,
        sales.created_at
      FROM 
        sales
      JOIN 
        products ON sales.product_id = products.id
      WHERE 
        DATE_TRUNC('day', sales.created_at) = $1
      `,
      [`${year}-${month}-${day}`]
    )

    const daySaleId = `day_sale_${year}_${month}_${day}`

    const totalUnitsSold = sales.rows.reduce(
      (total, sale) => total + sale.quantity,
      0
    )
    const totalRevenue = sales.rows.reduce(
      (total, sale) => total + sale.total_price,
      0
    )

    res.status(200).json({
      sales: sales.rows,
      total_units_sold: totalUnitsSold,
      total_revenue: totalRevenue,
      day_sale_id: daySaleId,
    })
  } catch (error) {
    console.error("Error al obtener resumen de ventas por día:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

const getSalesSummaryByMonth = async (req, res) => {
  try {
    const { year, month } = req.params

    const sales = await db.query(
      `
      SELECT 
        sales.id AS sale_id,
        products.id AS product_id,
        products.name AS product_name,
        products.description AS product_description,
        products.price AS product_price,
        sales.quantity,
        sales.total_price,
        sales.created_at
      FROM 
        sales
      JOIN 
        products ON sales.product_id = products.id
      WHERE 
        EXTRACT(YEAR FROM sales.created_at) = $1
        AND EXTRACT(MONTH FROM sales.created_at) = $2
      `,
      [year, month]
    )

    const salesByDay = {}
    sales.rows.forEach((sale) => {
      const day = sale.created_at.getDate()
      if (!salesByDay[day]) {
        salesByDay[day] = []
      }
      salesByDay[day].push(sale)
    })

    const salesSummaryByDay = {}
    Object.keys(salesByDay).forEach((day) => {
      const totalUnitsSold = salesByDay[day].reduce(
        (total, sale) => total + sale.quantity,
        0
      )
      const totalRevenue = salesByDay[day].reduce(
        (total, sale) => total + sale.total_price,
        0
      )
      const daySaleId = `day_sale_${year}_${month}_${day}`
      salesSummaryByDay[daySaleId] = {
        total_units_sold: totalUnitsSold,
        total_revenue: totalRevenue,
        sales: salesByDay[day],
      }
    })

    const totalUnitsSoldByMonth = sales.rows.reduce(
      (total, sale) => total + sale.quantity,
      0
    )
    const totalRevenueByMonth = sales.rows.reduce(
      (total, sale) => total + sale.total_price,
      0
    )

    res.status(200).json({
      sales_by_day: salesSummaryByDay,
      total_units_sold_by_month: totalUnitsSoldByMonth,
      total_revenue_by_month: totalRevenueByMonth,
    })
  } catch (error) {
    console.error("Error al obtener resumen de ventas por mes:", error)
    res.status(500).json({ message: "Error interno del servidor" })
  }
}

module.exports = {
  createSale,
  listSales,
  updateSale,
  deleteSale,
  getSalesByDay,
  getSalesSummaryByMonth,
}
