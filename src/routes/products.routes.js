import { Router } from "express"
import * as productController from "../controllers/product.controller"
import { authJwt } from "../middlewares"

const router = Router()

router.post(
  "/products",
  [authJwt.verifyToken, authJwt.isSupervisor],
  productController.createProduct
)
router.get("/products", authJwt.verifyToken, productController.listProducts)
router.get(
  "/products/:name",
  authJwt.verifyToken,
  productController.getProductByName
)
router.put(
  "/products/:id",
  [authJwt.verifyToken, authJwt.isSupervisor],
  productController.updateProduct
)
router.delete(
  "/products/:id",
  [authJwt.verifyToken, authJwt.isAdmin],
  productController.deleteProduct
)

export default router
