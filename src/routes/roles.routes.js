import { Router } from "express"
import * as roleController from "../controllers/roles.controller"
import { authJwt } from "../middlewares"

const router = Router()

router.post(
  "/roles",
  [authJwt.verifyToken, authJwt.isAdmin],
  roleController.createRole
)
router.get("/roles", [authJwt.verifyToken, authJwt.isAdmin], roleController.listRoles)
router.delete(
  "/roles/:id",
  [authJwt.verifyToken, authJwt.isAdmin],
  roleController.deleteRole
)

export default router
