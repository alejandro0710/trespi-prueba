import { Router } from "express"
import saleController from "../controllers/sales.controller"
import { authJwt } from "../middlewares"

const router = Router()

router.post("/sales", authJwt.verifyToken, saleController.createSale)
router.get(
  "/sales",
  [authJwt.verifyToken, authJwt.isSupervisor],
  saleController.listSales
)
router.put(
  "/sales/:id",
  [authJwt.verifyToken, authJwt.isSupervisor],
  saleController.updateSale
)
router.delete(
  "/sales/:id",
  [authJwt.verifyToken, authJwt.isAdmin],
  saleController.deleteSale
)
router.get(
  "/sales/:year/:month/:day",
  [authJwt.verifyToken, authJwt.isAdmin],
  saleController.getSalesByDay
)
router.get(
  "/sales-summary/:year/:month",
  [authJwt.verifyToken, authJwt.isAdmin],
  saleController.getSalesSummaryByMonth
)

module.exports = router
