import { Router } from "express"
import * as userController from "../controllers/user.controller"
import { authJwt } from "../middlewares"

const router = Router()

router.post("/signup", userController.signup)
router.post("/login", userController.login)

router.get(
  "/users",
  [authJwt.verifyToken, authJwt.isAdmin],
  userController.listUsers
)
router.get(
  "/users/:id",
  [authJwt.verifyToken, authJwt.isAdmin],
  userController.getUserById
)
router.put(
  "/users/:id",
  [authJwt.verifyToken, authJwt.isSupervisor],
  userController.updateUser
)
router.delete(
  "/users/:id",
  [authJwt.verifyToken, authJwt.isAdmin],
  userController.deleteUser
)
router.post(
  "/users/:id/assign-role",
  [authJwt.verifyToken, authJwt.isAdmin],
  userController.assignRole
)

module.exports = router

export default router
