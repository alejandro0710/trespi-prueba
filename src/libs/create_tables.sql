CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS roles (
  id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role_id UUID REFERENCES roles(id)
);

CREATE TABLE IF NOT EXISTS user_roles (
  id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  user_id UUID REFERENCES users(id) ON DELETE CASCADE,
  role_id UUID REFERENCES roles(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS products (
  id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  price INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS sales (
  id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  product_id UUID REFERENCES products(id) NOT NULL,
  quantity INTEGER NOT NULL,
  total_price INTEGER NOT NULL,
  created_at TIMESTAMP DEFAULT NOW()
);

-- Insertar roles si no existen
INSERT INTO roles (id, name) VALUES
  ('479fba0a-baaf-4b46-95a2-83a663817646', 'admin'),
  ('efbff7f6-6374-4c2f-9c96-3611c65068ba', 'supervisor'),
  ('f7c377cf-0f92-435a-b5e6-2c8cdd9d10c6', 'employee')
ON CONFLICT (id) DO NOTHING;

-- Insertar productos si no existen
INSERT INTO products (id, name, description, price) VALUES
  ('479fba0a-baaf-4b46-95a2-83a663817646', 'Arroz', 'Libra', 3000),
  ('efbff7f6-6374-4c2f-9c96-3611c65068ba', 'Papas', 'Libra', 1000),
  ('f7c377cf-0f92-435a-b5e6-2c8cdd9d10c6', 'Agua sin gas', '500 ml', 2000),
  ('3bed5d90-64ed-4bc1-8a3a-a378737ed542', 'Agua con gas', '500 ml', 2500),
  ('c3f25f98-c5c3-4a00-b550-f716ae36b25f', 'Docena de huevos', 'ministro de hacienda aprueba', 1800);
