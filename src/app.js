import express from "express"
import morgan from "morgan"
import pkg from "../package.json"
import productsRoutes from "./routes/products.routes"
import authRoutes from "./routes/auth.routes"
import rolesRoutes from "./routes/roles.routes"
import salesRoutes from "./routes/sales.routes"

const app = express()

app.set("pkg", pkg)
app.use(morgan("dev"))
app.use(express.json())

app.get("/", (req, res) => {
  res.json({
    name: app.get("pkg").name,
    author: app.get("pkg").author,
    description: app.get("pkg").description,
    version: app.get("pkg").version,
  })
})

app.use(productsRoutes)
app.use(authRoutes)
app.use(rolesRoutes)
app.use(salesRoutes)

export default app
