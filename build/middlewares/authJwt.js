"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _this = this;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

require("dotenv").config();

var verifyToken = function verifyToken(req, res, next) {
  var secretKey, token, decoded, user;
  return regeneratorRuntime.async(function verifyToken$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        secretKey = process.env.SECRET_KEY;
        token = req.headers["x-access-token"];

        if (token) {
          context$1$0.next = 4;
          break;
        }

        return context$1$0.abrupt("return", res.status(403).json({ message: "No se proporciona ningún token" }));

      case 4:
        context$1$0.prev = 4;
        decoded = _jsonwebtoken2["default"].verify(token, secretKey);
        context$1$0.next = 8;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE id = $1", [decoded.id]));

      case 8:
        user = context$1$0.sent;

        req.user = user.rows[0];
        next();
        context$1$0.next = 16;
        break;

      case 13:
        context$1$0.prev = 13;
        context$1$0.t0 = context$1$0["catch"](4);
        return context$1$0.abrupt("return", res.status(401).json({ message: "Ningún usuario encontrado" }));

      case 16:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[4, 13]]);
};

exports.verifyToken = verifyToken;
var isSupervisor = function isSupervisor(req, res, next) {
  var userId, user, rolesQuery, userRolesIds, rolesNamesQuery, userRoles;
  return regeneratorRuntime.async(function isSupervisor$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.user.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE id = $1", [userId]));

      case 4:
        user = context$1$0.sent;

        if (!(user.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Usuario no encontrado" }));

      case 7:
        context$1$0.next = 9;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT role_id FROM user_roles WHERE user_id = $1", [userId]));

      case 9:
        rolesQuery = context$1$0.sent;
        userRolesIds = rolesQuery.rows.map(function (role) {
          return role.role_id;
        });
        context$1$0.next = 13;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT name FROM roles WHERE id = ANY($1)", [userRolesIds]));

      case 13:
        rolesNamesQuery = context$1$0.sent;
        userRoles = rolesNamesQuery.rows.map(function (role) {
          return role.name;
        });

        if (!userRoles.includes("supervisor")) {
          context$1$0.next = 19;
          break;
        }

        next();
        context$1$0.next = 20;
        break;

      case 19:
        return context$1$0.abrupt("return", res.status(403).json({
          message: "Acceso no autorizado. Se requiere rol de supervisor"
        }));

      case 20:
        context$1$0.next = 26;
        break;

      case 22:
        context$1$0.prev = 22;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al verificar rol de supervisor:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 26:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 22]]);
};

exports.isSupervisor = isSupervisor;
var isAdmin = function isAdmin(req, res, next) {
  var userId, user, rolesQuery, userRolesIds, rolesNamesQuery, userRoles;
  return regeneratorRuntime.async(function isAdmin$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.user.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE id = $1", [userId]));

      case 4:
        user = context$1$0.sent;

        if (!(user.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Usuario no encontrado" }));

      case 7:
        context$1$0.next = 9;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT role_id FROM user_roles WHERE user_id = $1", [userId]));

      case 9:
        rolesQuery = context$1$0.sent;
        userRolesIds = rolesQuery.rows.map(function (role) {
          return role.role_id;
        });
        context$1$0.next = 13;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT name FROM roles WHERE id = ANY($1)", [userRolesIds]));

      case 13:
        rolesNamesQuery = context$1$0.sent;
        userRoles = rolesNamesQuery.rows.map(function (role) {
          return role.name;
        });

        if (!userRoles.includes("admin")) {
          context$1$0.next = 19;
          break;
        }

        next();
        context$1$0.next = 20;
        break;

      case 19:
        return context$1$0.abrupt("return", res.status(403).json({
          message: "Acceso no autorizado. Se requiere rol de administrador"
        }));

      case 20:
        context$1$0.next = 26;
        break;

      case 22:
        context$1$0.prev = 22;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al verificar rol de administrador:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 26:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 22]]);
};
exports.isAdmin = isAdmin;