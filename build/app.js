"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _morgan = require("morgan");

var _morgan2 = _interopRequireDefault(_morgan);

var _packageJson = require("../package.json");

var _packageJson2 = _interopRequireDefault(_packageJson);

var _routesProductsRoutes = require("./routes/products.routes");

var _routesProductsRoutes2 = _interopRequireDefault(_routesProductsRoutes);

var _routesAuthRoutes = require("./routes/auth.routes");

var _routesAuthRoutes2 = _interopRequireDefault(_routesAuthRoutes);

var _routesRolesRoutes = require("./routes/roles.routes");

var _routesRolesRoutes2 = _interopRequireDefault(_routesRolesRoutes);

var _routesSalesRoutes = require("./routes/sales.routes");

var _routesSalesRoutes2 = _interopRequireDefault(_routesSalesRoutes);

var app = (0, _express2["default"])();

app.set("pkg", _packageJson2["default"]);
app.use((0, _morgan2["default"])("dev"));
app.use(_express2["default"].json());

app.get("/", function (req, res) {
  res.json({
    name: app.get("pkg").name,
    author: app.get("pkg").author,
    description: app.get("pkg").description,
    version: app.get("pkg").version
  });
});

app.use(_routesProductsRoutes2["default"]);
app.use(_routesAuthRoutes2["default"]);
app.use(_routesRolesRoutes2["default"]);
app.use(_routesSalesRoutes2["default"]);

exports["default"] = app;
module.exports = exports["default"];