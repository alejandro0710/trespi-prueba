"use strict";

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _app = require("./app");

var _app2 = _interopRequireDefault(_app);

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

_app2["default"].listen(3000);
console.log("Servidor escuchando en http://localhost:", 3000);