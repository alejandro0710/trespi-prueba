"use strict";

var _this = this;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

var _uuid = require("uuid");

var createRole = function createRole(req, res) {
  var _name, existingRole, roleId, newRole;

  return regeneratorRuntime.async(function createRole$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _name = req.body.name;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM roles WHERE name = $1", [_name]));

      case 4:
        existingRole = context$1$0.sent;

        if (!(existingRole.rows.length > 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(400).json({ message: "El rol ya existe" }));

      case 7:
        roleId = (0, _uuid.v4)();
        context$1$0.next = 10;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO roles (id, name) VALUES ($1, $2) RETURNING *", [roleId, _name]));

      case 10:
        newRole = context$1$0.sent;

        res.status(201).json(newRole.rows[0]);
        context$1$0.next = 18;
        break;

      case 14:
        context$1$0.prev = 14;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al crear rol:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 18:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 14]]);
};

var listRoles = function listRoles(req, res) {
  var roles;
  return regeneratorRuntime.async(function listRoles$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM roles"));

      case 3:
        roles = context$1$0.sent;

        res.status(200).json(roles.rows);
        context$1$0.next = 11;
        break;

      case 7:
        context$1$0.prev = 7;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al listar roles:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 11:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 7]]);
};

var deleteRole = function deleteRole(req, res) {
  var roleId, existingRole;
  return regeneratorRuntime.async(function deleteRole$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        roleId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM roles WHERE id = $1", [roleId]));

      case 4:
        existingRole = context$1$0.sent;

        if (!(existingRole.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Rol no encontrado" }));

      case 7:
        context$1$0.next = 9;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM roles WHERE id = $1", [roleId]));

      case 9:

        res.status(200).json("Rol con ID " + roleId + " eliminado correctamente.");
        context$1$0.next = 16;
        break;

      case 12:
        context$1$0.prev = 12;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al borrar rol:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 16:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 12]]);
};

module.exports = {
  createRole: createRole,
  listRoles: listRoles,
  deleteRole: deleteRole
};