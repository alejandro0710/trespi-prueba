"use strict";

var _this = this;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

var _uuid = require("uuid");

var createProduct = function createProduct(req, res) {
  var _req$body, _name, description, price, existingProduct, productId, insertQuery, newProduct;

  return regeneratorRuntime.async(function createProduct$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _req$body = req.body;
        _name = _req$body.name;
        description = _req$body.description;
        price = _req$body.price;
        context$1$0.next = 7;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM products WHERE name = $1", [_name]));

      case 7:
        existingProduct = context$1$0.sent;

        if (!(existingProduct.rows.length > 0)) {
          context$1$0.next = 10;
          break;
        }

        return context$1$0.abrupt("return", res.status(400).json({ message: "El producto ya existe" }));

      case 10:
        productId = (0, _uuid.v4)();
        insertQuery = "\n        INSERT INTO products (id, name, description, price)\n        VALUES ($1, $2, $3, $4)\n        RETURNING *\n      ";
        context$1$0.next = 14;
        return regeneratorRuntime.awrap(_configJs2["default"].query(insertQuery, [productId, _name, description, price]));

      case 14:
        newProduct = context$1$0.sent;

        res.status(201).json(newProduct.rows[0]);
        context$1$0.next = 22;
        break;

      case 18:
        context$1$0.prev = 18;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al crear producto:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 22:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 18]]);
};

var listProducts = function listProducts(req, res) {
  var products;
  return regeneratorRuntime.async(function listProducts$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM products"));

      case 3:
        products = context$1$0.sent;

        res.status(200).json(products.rows);
        context$1$0.next = 11;
        break;

      case 7:
        context$1$0.prev = 7;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al listar productos:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 11:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 7]]);
};

var getProductByName = function getProductByName(req, res) {
  var _name2, product;

  return regeneratorRuntime.async(function getProductByName$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _name2 = req.params.name;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM products WHERE LOWER(name) = LOWER($1)", [_name2]));

      case 4:
        product = context$1$0.sent;

        if (!(product.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Producto no encontrado" }));

      case 7:

        res.status(200).json(product.rows[0]);
        context$1$0.next = 14;
        break;

      case 10:
        context$1$0.prev = 10;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al obtener producto por nombre:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 14:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 10]]);
};

var updateProduct = function updateProduct(req, res) {
  var productId, existingProduct, old, _req$body2, _req$body2$name, _name3, _req$body2$description, description, _req$body2$price, price, updatedProduct;

  return regeneratorRuntime.async(function updateProduct$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        productId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM products WHERE id = $1", [productId]));

      case 4:
        existingProduct = context$1$0.sent;
        old = existingProduct.rows[0];
        _req$body2 = req.body;
        _req$body2$name = _req$body2.name;
        _name3 = _req$body2$name === undefined ? old.name : _req$body2$name;
        _req$body2$description = _req$body2.description;
        description = _req$body2$description === undefined ? old.description : _req$body2$description;
        _req$body2$price = _req$body2.price;
        price = _req$body2$price === undefined ? old.price : _req$body2$price;

        if (!(existingProduct.rows.length === 0)) {
          context$1$0.next = 15;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Producto no encontrado" }));

      case 15:
        context$1$0.next = 17;
        return regeneratorRuntime.awrap(_configJs2["default"].query("UPDATE products SET name = $1, description = $2, price = $3 WHERE id = $4 RETURNING *", [_name3, description, price, productId]));

      case 17:
        updatedProduct = context$1$0.sent;

        res.status(200).json(updatedProduct.rows[0]);
        context$1$0.next = 25;
        break;

      case 21:
        context$1$0.prev = 21;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al actualizar producto:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 25:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 21]]);
};

var deleteProduct = function deleteProduct(req, res) {
  var productId, existingProduct;
  return regeneratorRuntime.async(function deleteProduct$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        productId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM products WHERE id = $1", [productId]));

      case 4:
        existingProduct = context$1$0.sent;

        if (!(existingProduct.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Producto no encontrado" }));

      case 7:
        context$1$0.next = 9;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM products WHERE id = $1", [productId]));

      case 9:

        res.status(200).json("Producto con ID " + productId + " eliminado correctamente.");
        context$1$0.next = 16;
        break;

      case 12:
        context$1$0.prev = 12;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al borrar producto:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 16:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 12]]);
};

module.exports = {
  createProduct: createProduct,
  listProducts: listProducts,
  updateProduct: updateProduct,
  deleteProduct: deleteProduct,
  getProductByName: getProductByName
};