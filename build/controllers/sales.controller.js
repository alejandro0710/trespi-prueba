"use strict";

var _this = this;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

var createSale = function createSale(req, res) {
  var _req$body, productId, quantity, product, _product$rows$0, id, _name, description, price, totalPrice, sale, formattedDate;

  return regeneratorRuntime.async(function createSale$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _req$body = req.body;
        productId = _req$body.productId;
        quantity = _req$body.quantity;
        context$1$0.next = 6;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT id, name, description, price FROM products WHERE id = $1", [productId]));

      case 6:
        product = context$1$0.sent;

        if (product.rows.length) {
          context$1$0.next = 9;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Producto no encontrado" }));

      case 9:
        _product$rows$0 = product.rows[0];
        id = _product$rows$0.id;
        _name = _product$rows$0.name;
        description = _product$rows$0.description;
        price = _product$rows$0.price;
        totalPrice = quantity * price;
        context$1$0.next = 17;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO sales (product_id, quantity, total_price, created_at) VALUES ($1, $2, $3, NOW() AT TIME ZONE 'America/Bogota') RETURNING *", [id, quantity, totalPrice]));

      case 17:
        sale = context$1$0.sent;
        formattedDate = new Intl.DateTimeFormat("es-CO", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit"
        }).format(new Date(sale.rows[0].created_at));

        res.status(201).json(_extends({}, sale.rows[0], {
          productName: _name,
          productDescription: description,
          formattedDate: formattedDate
        }));
        context$1$0.next = 26;
        break;

      case 22:
        context$1$0.prev = 22;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al crear venta:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 26:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 22]]);
};

var listSales = function listSales(req, res) {
  var sales;
  return regeneratorRuntime.async(function listSales$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT sales.*, products.name as product_name, products.description as product_description FROM sales INNER JOIN products ON sales.product_id = products.id"));

      case 3:
        sales = context$1$0.sent;

        res.status(200).json(sales.rows);
        context$1$0.next = 11;
        break;

      case 7:
        context$1$0.prev = 7;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al listar ventas:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 11:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 7]]);
};

var updateSale = function updateSale(req, res) {
  var saleId, quantity, existingSale, productInfo, unitPrice, totalPrice, updatedSale, formattedDate;
  return regeneratorRuntime.async(function updateSale$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        saleId = req.params.id;
        quantity = req.body.quantity;
        context$1$0.next = 5;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM sales WHERE id = $1", [saleId]));

      case 5:
        existingSale = context$1$0.sent;

        if (!(existingSale.rows.length === 0)) {
          context$1$0.next = 8;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Venta no encontrada" }));

      case 8:
        context$1$0.next = 10;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT products.name AS product_name, products.description AS product_description, products.price FROM sales JOIN products ON sales.product_id = products.id WHERE sales.id = $1", [saleId]));

      case 10:
        productInfo = context$1$0.sent;

        if (!(productInfo.rows.length === 0)) {
          context$1$0.next = 13;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Información del producto asociado no encontrada" }));

      case 13:
        unitPrice = productInfo.rows[0].price;
        totalPrice = quantity * unitPrice;
        context$1$0.next = 17;
        return regeneratorRuntime.awrap(_configJs2["default"].query("UPDATE sales SET quantity = $1, total_price = $2, created_at = NOW() AT TIME ZONE 'America/Bogota' WHERE id = $3 RETURNING *", [quantity, totalPrice, saleId]));

      case 17:
        updatedSale = context$1$0.sent;

        updatedSale.rows[0].product_name = productInfo.rows[0].product_name;
        updatedSale.rows[0].product_description = productInfo.rows[0].product_description;

        formattedDate = new Intl.DateTimeFormat("es-CO", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit"
        }).format(new Date(updatedSale.rows[0].created_at));

        updatedSale.rows[0].created_at = formattedDate;

        res.status(200).json(updatedSale.rows[0]);
        context$1$0.next = 29;
        break;

      case 25:
        context$1$0.prev = 25;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al actualizar venta:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 29:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 25]]);
};

var deleteSale = function deleteSale(req, res) {
  var saleId, existingSale;
  return regeneratorRuntime.async(function deleteSale$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        saleId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM sales WHERE id = $1", [saleId]));

      case 4:
        existingSale = context$1$0.sent;

        if (!(existingSale.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Venta no encontrada" }));

      case 7:
        context$1$0.next = 9;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM sales WHERE id = $1", [saleId]));

      case 9:

        res.status(200).json("Venta con ID " + saleId + " eliminada correctamente.");
        context$1$0.next = 16;
        break;

      case 12:
        context$1$0.prev = 12;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al borrar venta:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 16:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 12]]);
};

var getSalesByDay = function getSalesByDay(req, res) {
  var _req$params, year, month, day, sales, daySaleId, totalUnitsSold, totalRevenue;

  return regeneratorRuntime.async(function getSalesByDay$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _req$params = req.params;
        year = _req$params.year;
        month = _req$params.month;
        day = _req$params.day;
        context$1$0.next = 7;
        return regeneratorRuntime.awrap(_configJs2["default"].query("\n      SELECT \n        sales.id AS sale_id,\n        products.id AS product_id,\n        products.name AS product_name,\n        products.description AS product_description,\n        products.price AS product_price,\n        sales.quantity,\n        sales.total_price,\n        sales.created_at\n      FROM \n        sales\n      JOIN \n        products ON sales.product_id = products.id\n      WHERE \n        DATE_TRUNC('day', sales.created_at) = $1\n      ", [year + "-" + month + "-" + day]));

      case 7:
        sales = context$1$0.sent;
        daySaleId = "day_sale_" + year + "_" + month + "_" + day;
        totalUnitsSold = sales.rows.reduce(function (total, sale) {
          return total + sale.quantity;
        }, 0);
        totalRevenue = sales.rows.reduce(function (total, sale) {
          return total + sale.total_price;
        }, 0);

        res.status(200).json({
          sales: sales.rows,
          total_units_sold: totalUnitsSold,
          total_revenue: totalRevenue,
          day_sale_id: daySaleId
        });
        context$1$0.next = 18;
        break;

      case 14:
        context$1$0.prev = 14;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al obtener resumen de ventas por día:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 18:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 14]]);
};

var getSalesSummaryByMonth = function getSalesSummaryByMonth(req, res) {
  return regeneratorRuntime.async(function getSalesSummaryByMonth$(context$1$0) {
    var _this2 = this;

    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap((function callee$1$0() {
          var _req$params2, year, month, sales, salesByDay, salesSummaryByDay, totalUnitsSoldByMonth, totalRevenueByMonth;

          return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
            while (1) switch (context$2$0.prev = context$2$0.next) {
              case 0:
                _req$params2 = req.params;
                year = _req$params2.year;
                month = _req$params2.month;
                context$2$0.next = 5;
                return regeneratorRuntime.awrap(_configJs2["default"].query("\n      SELECT \n        sales.id AS sale_id,\n        products.id AS product_id,\n        products.name AS product_name,\n        products.description AS product_description,\n        products.price AS product_price,\n        sales.quantity,\n        sales.total_price,\n        sales.created_at\n      FROM \n        sales\n      JOIN \n        products ON sales.product_id = products.id\n      WHERE \n        EXTRACT(YEAR FROM sales.created_at) = $1\n        AND EXTRACT(MONTH FROM sales.created_at) = $2\n      ", [year, month]));

              case 5:
                sales = context$2$0.sent;
                salesByDay = {};

                sales.rows.forEach(function (sale) {
                  var day = sale.created_at.getDate();
                  if (!salesByDay[day]) {
                    salesByDay[day] = [];
                  }
                  salesByDay[day].push(sale);
                });

                salesSummaryByDay = {};

                Object.keys(salesByDay).forEach(function (day) {
                  var totalUnitsSold = salesByDay[day].reduce(function (total, sale) {
                    return total + sale.quantity;
                  }, 0);
                  var totalRevenue = salesByDay[day].reduce(function (total, sale) {
                    return total + sale.total_price;
                  }, 0);
                  var daySaleId = "day_sale_" + year + "_" + month + "_" + day;
                  salesSummaryByDay[daySaleId] = {
                    total_units_sold: totalUnitsSold,
                    total_revenue: totalRevenue,
                    sales: salesByDay[day]
                  };
                });

                totalUnitsSoldByMonth = sales.rows.reduce(function (total, sale) {
                  return total + sale.quantity;
                }, 0);
                totalRevenueByMonth = sales.rows.reduce(function (total, sale) {
                  return total + sale.total_price;
                }, 0);

                res.status(200).json({
                  sales_by_day: salesSummaryByDay,
                  total_units_sold_by_month: totalUnitsSoldByMonth,
                  total_revenue_by_month: totalRevenueByMonth
                });

              case 13:
              case "end":
                return context$2$0.stop();
            }
          }, null, _this2);
        })());

      case 3:
        context$1$0.next = 9;
        break;

      case 5:
        context$1$0.prev = 5;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al obtener resumen de ventas por mes:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 9:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this, [[0, 5]]);
};

module.exports = {
  createSale: createSale,
  listSales: listSales,
  updateSale: updateSale,
  deleteSale: deleteSale,
  getSalesByDay: getSalesByDay,
  getSalesSummaryByMonth: getSalesSummaryByMonth
};