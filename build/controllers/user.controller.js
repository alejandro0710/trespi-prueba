"use strict";

var _this2 = this;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

var _bcryptjs = require("bcryptjs");

var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

require("dotenv").config();
var signup = function signup(req, res) {
  var _req$body, username, password, _req$body$roles, roles, existingUser, hashedPassword, roleIds, newUser, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, roleId;

  return regeneratorRuntime.async(function signup$(context$1$0) {
    var _this = this;

    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _req$body = req.body;
        username = _req$body.username;
        password = _req$body.password;
        _req$body$roles = _req$body.roles;
        roles = _req$body$roles === undefined ? ["employee"] : _req$body$roles;
        context$1$0.next = 8;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE username = $1", [username]));

      case 8:
        existingUser = context$1$0.sent;

        if (!(existingUser.rows.length > 0)) {
          context$1$0.next = 11;
          break;
        }

        return context$1$0.abrupt("return", res.status(400).json({ message: "El usuario ya existe" }));

      case 11:
        context$1$0.next = 13;
        return regeneratorRuntime.awrap(_bcryptjs2["default"].hash(password, 10));

      case 13:
        hashedPassword = context$1$0.sent;
        context$1$0.next = 16;
        return regeneratorRuntime.awrap(Promise.all(roles.map(function callee$1$0(roleName) {
          var result;
          return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
            while (1) switch (context$2$0.prev = context$2$0.next) {
              case 0:
                context$2$0.next = 2;
                return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT id FROM roles WHERE name = $1", [roleName]));

              case 2:
                result = context$2$0.sent;
                return context$2$0.abrupt("return", result.rows[0] ? result.rows[0].id : undefined);

              case 4:
              case "end":
                return context$2$0.stop();
            }
          }, null, _this);
        })));

      case 16:
        roleIds = context$1$0.sent;

        if (!roleIds.some(function (id) {
          return id === undefined;
        })) {
          context$1$0.next = 19;
          break;
        }

        return context$1$0.abrupt("return", res.status(400).json({ message: "Al menos uno de los roles proporcionados no existe" }));

      case 19:
        context$1$0.next = 21;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO users (id, username, password, roles) VALUES (uuid_generate_v4(), $1, $2, $3) RETURNING *", [username, hashedPassword, roles[0]]));

      case 21:
        newUser = context$1$0.sent;
        _iteratorNormalCompletion = true;
        _didIteratorError = false;
        _iteratorError = undefined;
        context$1$0.prev = 25;
        _iterator = roleIds[Symbol.iterator]();

      case 27:
        if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
          context$1$0.next = 34;
          break;
        }

        roleId = _step.value;
        context$1$0.next = 31;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)", [newUser.rows[0].id, roleId]));

      case 31:
        _iteratorNormalCompletion = true;
        context$1$0.next = 27;
        break;

      case 34:
        context$1$0.next = 40;
        break;

      case 36:
        context$1$0.prev = 36;
        context$1$0.t0 = context$1$0["catch"](25);
        _didIteratorError = true;
        _iteratorError = context$1$0.t0;

      case 40:
        context$1$0.prev = 40;
        context$1$0.prev = 41;

        if (!_iteratorNormalCompletion && _iterator["return"]) {
          _iterator["return"]();
        }

      case 43:
        context$1$0.prev = 43;

        if (!_didIteratorError) {
          context$1$0.next = 46;
          break;
        }

        throw _iteratorError;

      case 46:
        return context$1$0.finish(43);

      case 47:
        return context$1$0.finish(40);

      case 48:

        res.status(201).json(newUser.rows[0]);
        context$1$0.next = 55;
        break;

      case 51:
        context$1$0.prev = 51;
        context$1$0.t1 = context$1$0["catch"](0);

        console.error("Error al crear usuario:", context$1$0.t1);
        res.status(500).json({ message: "Error interno del servidor" });

      case 55:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 51], [25, 36, 40, 48], [41,, 43, 47]]);
};
var login = function login(req, res) {
  var _req$body2, username, password, user, passwordMatch, userId, userRoles, roleIds, roles, roleNames, secretKey, token;

  return regeneratorRuntime.async(function login$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        _req$body2 = req.body;
        username = _req$body2.username;
        password = _req$body2.password;
        context$1$0.next = 6;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE username = $1", [username]));

      case 6:
        user = context$1$0.sent;

        if (!(user.rows.length === 0)) {
          context$1$0.next = 9;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Usuario no encontrado" }));

      case 9:
        context$1$0.next = 11;
        return regeneratorRuntime.awrap(_bcryptjs2["default"].compare(password, user.rows[0].password));

      case 11:
        passwordMatch = context$1$0.sent;

        if (passwordMatch) {
          context$1$0.next = 14;
          break;
        }

        return context$1$0.abrupt("return", res.status(401).json({ message: "Contraseña incorrecta" }));

      case 14:
        userId = user.rows[0].id;
        context$1$0.next = 17;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT role_id FROM user_roles WHERE user_id = $1", [userId]));

      case 17:
        userRoles = context$1$0.sent;
        roleIds = userRoles.rows.map(function (row) {
          return row.role_id;
        });
        context$1$0.next = 21;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT name FROM roles WHERE id = ANY($1)", [roleIds]));

      case 21:
        roles = context$1$0.sent;
        roleNames = roles.rows.map(function (row) {
          return row.name;
        });
        secretKey = process.env.SECRET_KEY;
        token = _jsonwebtoken2["default"].sign({ id: userId }, secretKey, {
          expiresIn: 86400
        });

        res.status(200).json({
          message: "Inicio de sesión exitoso",
          token: token,
          user: _extends({}, user.rows[0], { roles: roleNames })
        });
        context$1$0.next = 32;
        break;

      case 28:
        context$1$0.prev = 28;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al iniciar sesión:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 32:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 28]]);
};

var listUsers = function listUsers(req, res) {
  var users;
  return regeneratorRuntime.async(function listUsers$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users"));

      case 3:
        users = context$1$0.sent;

        res.status(200).json(users.rows);
        context$1$0.next = 11;
        break;

      case 7:
        context$1$0.prev = 7;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al listar usuarios:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 11:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 7]]);
};

var updateUser = function updateUser(req, res) {
  var userId, existingUser, oldUser, _req$body3, _req$body3$username, username, _req$body3$password, password, _req$body3$roles, roles, hashedPassword, roleIds, updatedUser, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, roleId;

  return regeneratorRuntime.async(function updateUser$(context$1$0) {
    var _this3 = this;

    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE id = $1", [userId]));

      case 4:
        existingUser = context$1$0.sent;

        if (!(existingUser.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Usuario no encontrado" }));

      case 7:
        oldUser = existingUser.rows[0];
        _req$body3 = req.body;
        _req$body3$username = _req$body3.username;
        username = _req$body3$username === undefined ? oldUser.username : _req$body3$username;
        _req$body3$password = _req$body3.password;
        password = _req$body3$password === undefined ? oldUser.password : _req$body3$password;
        _req$body3$roles = _req$body3.roles;
        roles = _req$body3$roles === undefined ? [oldUser.roles] : _req$body3$roles;
        context$1$0.next = 17;
        return regeneratorRuntime.awrap(_bcryptjs2["default"].hash(password, 10));

      case 17:
        hashedPassword = context$1$0.sent;
        context$1$0.next = 20;
        return regeneratorRuntime.awrap(Promise.all(roles.map(function callee$1$0(roleName) {
          var result;
          return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
            while (1) switch (context$2$0.prev = context$2$0.next) {
              case 0:
                context$2$0.next = 2;
                return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT id FROM roles WHERE name = $1", [roleName]));

              case 2:
                result = context$2$0.sent;
                return context$2$0.abrupt("return", result.rows[0] ? result.rows[0].id : undefined);

              case 4:
              case "end":
                return context$2$0.stop();
            }
          }, null, _this3);
        })));

      case 20:
        roleIds = context$1$0.sent;

        if (!roleIds.some(function (id) {
          return id === undefined;
        })) {
          context$1$0.next = 23;
          break;
        }

        return context$1$0.abrupt("return", res.status(400).json({ message: "Al menos uno de los roles proporcionados no existe" }));

      case 23:
        context$1$0.next = 25;
        return regeneratorRuntime.awrap(_configJs2["default"].query("UPDATE users SET username = $1, password = $2, roles = $3 WHERE id = $4 RETURNING *", [username, hashedPassword, roleIds, userId]));

      case 25:
        updatedUser = context$1$0.sent;
        context$1$0.next = 28;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM user_roles WHERE user_id = $1", [userId]));

      case 28:
        _iteratorNormalCompletion2 = true;
        _didIteratorError2 = false;
        _iteratorError2 = undefined;
        context$1$0.prev = 31;
        _iterator2 = roleIds[Symbol.iterator]();

      case 33:
        if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
          context$1$0.next = 40;
          break;
        }

        roleId = _step2.value;
        context$1$0.next = 37;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)", [userId, roleId]));

      case 37:
        _iteratorNormalCompletion2 = true;
        context$1$0.next = 33;
        break;

      case 40:
        context$1$0.next = 46;
        break;

      case 42:
        context$1$0.prev = 42;
        context$1$0.t0 = context$1$0["catch"](31);
        _didIteratorError2 = true;
        _iteratorError2 = context$1$0.t0;

      case 46:
        context$1$0.prev = 46;
        context$1$0.prev = 47;

        if (!_iteratorNormalCompletion2 && _iterator2["return"]) {
          _iterator2["return"]();
        }

      case 49:
        context$1$0.prev = 49;

        if (!_didIteratorError2) {
          context$1$0.next = 52;
          break;
        }

        throw _iteratorError2;

      case 52:
        return context$1$0.finish(49);

      case 53:
        return context$1$0.finish(46);

      case 54:

        res.status(200).json(updatedUser.rows[0]);
        context$1$0.next = 61;
        break;

      case 57:
        context$1$0.prev = 57;
        context$1$0.t1 = context$1$0["catch"](0);

        console.error("Error al actualizar usuario:", context$1$0.t1);
        res.status(500).json({ message: "Error interno del servidor" });

      case 61:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 57], [31, 42, 46, 54], [47,, 49, 53]]);
};

var getUserById = function getUserById(req, res) {
  var userId, user;
  return regeneratorRuntime.async(function getUserById$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("SELECT * FROM users WHERE id = $1", [userId]));

      case 4:
        user = context$1$0.sent;

        if (!(user.rows.length === 0)) {
          context$1$0.next = 7;
          break;
        }

        return context$1$0.abrupt("return", res.status(404).json({ message: "Usuario no encontrado" }));

      case 7:

        res.status(200).json(user.rows[0]);
        context$1$0.next = 14;
        break;

      case 10:
        context$1$0.prev = 10;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al obtener usuario por ID:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 14:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 10]]);
};

var deleteUser = function deleteUser(req, res) {
  var userId;
  return regeneratorRuntime.async(function deleteUser$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.params.id;
        context$1$0.next = 4;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM user_roles WHERE user_id = $1", [userId]));

      case 4:
        context$1$0.next = 6;
        return regeneratorRuntime.awrap(_configJs2["default"].query("DELETE FROM users WHERE id = $1", [userId]));

      case 6:

        res.status(200).json("Usuario con ID " + userId + " eliminado correctamente.");
        context$1$0.next = 13;
        break;

      case 9:
        context$1$0.prev = 9;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al borrar usuario:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 13:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 9]]);
};

var assignRole = function assignRole(req, res) {
  var userId, roleId;
  return regeneratorRuntime.async(function assignRole$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        userId = req.params.id;
        roleId = req.body.roleId;
        context$1$0.next = 5;
        return regeneratorRuntime.awrap(_configJs2["default"].query("INSERT INTO user_roles (user_id, role_id) VALUES ($1, $2)", [userId, roleId]));

      case 5:

        res.status(200).json("Rol asignado correctamente al usuario con ID " + userId + ".");
        context$1$0.next = 12;
        break;

      case 8:
        context$1$0.prev = 8;
        context$1$0.t0 = context$1$0["catch"](0);

        console.error("Error al asignar rol al usuario:", context$1$0.t0);
        res.status(500).json({ message: "Error interno del servidor" });

      case 12:
      case "end":
        return context$1$0.stop();
    }
  }, null, _this2, [[0, 8]]);
};

module.exports = {
  signup: signup,
  login: login,
  getUserById: getUserById,
  listUsers: listUsers,
  updateUser: updateUser,
  deleteUser: deleteUser,
  assignRole: assignRole
};