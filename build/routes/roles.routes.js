"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }

var _express = require("express");

var _controllersRolesController = require("../controllers/roles.controller");

var roleController = _interopRequireWildcard(_controllersRolesController);

var _middlewares = require("../middlewares");

var router = (0, _express.Router)();

router.post("/roles", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], roleController.createRole);
router.get("/roles", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], roleController.listRoles);
router["delete"]("/roles/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], roleController.deleteRole);

exports["default"] = router;
module.exports = exports["default"];