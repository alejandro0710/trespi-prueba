"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }

var _express = require("express");

var _controllersProductController = require("../controllers/product.controller");

var productController = _interopRequireWildcard(_controllersProductController);

var _middlewares = require("../middlewares");

var router = (0, _express.Router)();

router.post("/products", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isSupervisor], productController.createProduct);
router.get("/products", _middlewares.authJwt.verifyToken, productController.listProducts);
router.get("/products/:name", _middlewares.authJwt.verifyToken, productController.getProductByName);
router.put("/products/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isSupervisor], productController.updateProduct);
router["delete"]("/products/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], productController.deleteProduct);

exports["default"] = router;
module.exports = exports["default"];