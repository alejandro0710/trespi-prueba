"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj["default"] = obj; return newObj; } }

var _express = require("express");

var _controllersUserController = require("../controllers/user.controller");

var userController = _interopRequireWildcard(_controllersUserController);

var _middlewares = require("../middlewares");

var router = (0, _express.Router)();

router.post("/signup", userController.signup);
router.post("/login", userController.login);

router.get("/users", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], userController.listUsers);
router.get("/users/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], userController.getUserById);
router.put("/users/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isSupervisor], userController.updateUser);
router["delete"]("/users/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], userController.deleteUser);
router.post("/users/:id/assign-role", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], userController.assignRole);

module.exports = router;

exports["default"] = router;
module.exports = exports["default"];