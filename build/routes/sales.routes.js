"use strict";

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _express = require("express");

var _controllersSalesController = require("../controllers/sales.controller");

var _controllersSalesController2 = _interopRequireDefault(_controllersSalesController);

var _middlewares = require("../middlewares");

var router = (0, _express.Router)();

router.post("/sales", _middlewares.authJwt.verifyToken, _controllersSalesController2["default"].createSale);
router.get("/sales", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isSupervisor], _controllersSalesController2["default"].listSales);
router.put("/sales/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isSupervisor], _controllersSalesController2["default"].updateSale);
router["delete"]("/sales/:id", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], _controllersSalesController2["default"].deleteSale);
router.get("/sales/:year/:month/:day", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], _controllersSalesController2["default"].getSalesByDay);
router.get("/sales-summary/:year/:month", [_middlewares.authJwt.verifyToken, _middlewares.authJwt.isAdmin], _controllersSalesController2["default"].getSalesSummaryByMonth);

module.exports = router;