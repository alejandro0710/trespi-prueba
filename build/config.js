"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _pg = require("pg");

var _dotenv = require("dotenv");

var _dotenv2 = _interopRequireDefault(_dotenv);

_dotenv2["default"].config();

var db = new _pg.Client({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
});

db.connect();

exports["default"] = db;
module.exports = exports["default"];