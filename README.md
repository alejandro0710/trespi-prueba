README - TresPI Project
Este README documenta los pasos seguidos para desarrollar y configurar el proyecto TresPI, un backend que ofrece servicios de gestión de ventas, productos y roles de usuario. A continuación, se presenta una guía paso a paso sobre cómo configurar y ejecutar la aplicación.

Contenido
Introducción
Estructura del Proyecto
Configuración del Entorno
Configuración de la Base de Datos
Instalación de Dependencias
Ejecución del Proyecto
Uso de las Funciones Implementadas
Contribuciones
Información de Contacto
1. Introducción
TresPI es un proyecto backend desarrollado en Node.js con Express y PostgreSQL. Proporciona endpoints para gestionar ventas, productos y roles de usuario.

2. Estructura del Proyecto
src/: Contiene el código fuente de la aplicación.
controllers/: Controladores para la lógica de negocio.
libs/: Utilidades y scripts para inicializar y gestionar la base de datos.
routes/: Definición de rutas para Express.
app.js: Archivo principal de la aplicación.
config.js: Configuración de la base de datos y otros ajustes.
create_tables.sql: Archivo SQL para crear las tablas necesarias en la base de datos.
3. Configuración del Entorno
Crear un archivo .env en la raíz del proyecto con la siguiente información:

plaintext
Copy code
DB_USER=nombre_de_usuario
DB_PASSWORD=contraseña_de_usuario
DB_HOST=nombre_de_host
DB_DATABASE=nombre_de_base_de_datos
DB_PORT=puerto_de_base_de_datos
Reemplazar los valores con la información de tu base de datos PostgreSQL.

Configurar la base de datos:
Asegurarse de que PostgreSQL esté en funcionamiento y la configuración coincida con la información proporcionada en el archivo .env.

4. Configuración de la Base de Datos
Asegurarse de tener configurado su base de datos local con postgresSQL12

5. Instalación de Dependencias
Instalar las dependencias del proyecto:

bash
Copy code
npm install
6. Ejecución del Proyecto
Compilar el código fuente:

bash
Copy code
npm run build
Iniciar el servidor:

bash
Copy code
npm start
o
npm run dev
La aplicación estará disponible en http://localhost:3000.

7. Uso de las Funciones Implementadas
Crear Venta:

http
Copy code
POST /sales
Crear una nueva venta proporcionando productId y quantity en el cuerpo de la solicitud.

Listar Ventas:

http
Copy code
GET /sales
Obtener una lista de todas las ventas realizadas.

Actualizar Venta:

http
Copy code
PUT /sales/:id
Actualizar una venta existente proporcionando quantity en el cuerpo de la solicitud.

Eliminar Venta:

http
Copy code
DELETE /sales/:id
Eliminar una venta existente según su ID.

Obtener Roles:

http
Copy code
GET /roles
Obtener la lista de roles disponibles.

Otros Endpoints:
Explorar otros endpoints en routes/ para administrar productos y roles de usuario.

8. Contribuciones
Contribuciones bienvenidas. Si encuentras errores, mejoras o deseas agregar nuevas características, realiza un fork del proyecto y envía pull requests.

9. Información de Contacto
Para preguntas o asistencia adicional, contactar al propietario del repositorio:

Nombre: [Alejandro Ramos]
Correo Electrónico: [alejandro.ramosjf@gmail.com]